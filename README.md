# ZoomSense Web Client

A static OPA to display data collected by ZoomSense agents operating inside specified Zoom meetings, allowing for greater insight into what's happening in your Zoom meetings and their break-out rooms.

A static site powered by Vue.js and Firebase.

## Project setup

You will need to point this client at the firebase realtime db instance that you are using for your deployment. Adjust `src/db.js` to your firebase settings.

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

This builds into the `public` dir which is then hosted on firebase.

```
npm run build
```

### Deployment

We deploy using firebase hosting:

`firebase deploy`
