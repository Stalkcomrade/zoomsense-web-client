import { EventBus } from "@/components/event-bus.js";
import { auth } from "./db";

function refreshToken(vueInstance) {
  return new Promise(function (resolve, reject) {
    var refresh = vueInstance.$cookies.get("refresh");
    return auth
      .signInWithCustomToken(refresh)
      .then(loginData => {
        vueInstance.user = loginData.user;
        return loginData.user.getIdToken();
      })
      .then(idToken => {
        vueInstance.$cookies.set("session", idToken, "12d");
        return resolve(idToken);
      })
      .catch(error => {
        console.error("account error, sign in again", error);
        EventBus.$emit("start-logout");
        reject(new Error(error));
      });
  });
}

export default {

  getSessionToken: function (vueInstance) {
    return new Promise(function (resolve, reject) {
      if (vueInstance.$cookies.isKey("session")) {
        resolve(vueInstance.$cookies.get("session"));
      }
      else if (vueInstance.$cookies.isKey("refresh")) {
        refreshToken(vueInstance).then(token => {
          resolve(token);
        });
      }
      else {
        vueInstance.$cookies
          .keys()
          .forEach(cookie => vueInstance.$cookies.remove(cookie));
        reject(new Error("no tokens found"));
      }
    })
  }
}