import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import ManageDocs from "../views/docs/ManageDocs.vue";
import AddDoc from "../views/docs/AddDoc.vue";
import MeetingDetails from "../views/MeetingDetails.vue";
import Login from "../views/Login.vue";
import Anonymous from "../views/Anonymous.vue";
import Admin from "../views/Admin.vue"

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/scheduled",
  },
  {
    path: "/login",
    component: Login,
    name: "Login",
    props: (route) => ({
      token: route.query.token,
    }),
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/admin",
    name: "Admin",
    component: Admin,
  },
  {
    path: "/managedocs/:meetingId",
    name: "Manage Documents",
    component: ManageDocs,
    props: (route) => ({
      existingDoc: route.query.existingDoc
    })
  },
  {
    path: "/adddoc/:meetingId",
    name: "Add New Document",
    component: AddDoc,
    props: (route) => ({
      meetingId: route.params.meetingId,
    })
  },
  {
    path: "/scheduled",
    name: "Scheduled",
    component: Home,
  },
  {
    path: "/anonymous",
    name: "Anonymous",
    component: Anonymous,
  },
  {
    path: "/archive",
    name: "Archive",
    component: Home,
  },
  {
    path: "/testVis",
    name: "testVis",
    component: Home,
  },
  {
    path: "/:id/:history?",
    name: "Meeting",
    component: MeetingDetails,
    props: true,
  }
];

const router = new VueRouter({
  // mode: 'history',
  // base: process.env.BASE_URL,
  routes,
});

export default router;
