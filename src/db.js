import * as firebase from "firebase/app";
import * as firebaseui from "firebaseui";
import "firebase/database";
import "firebase/auth";
import "firebase/storage";
import "firebase/functions";
import "firebaseui";
import "firebaseui/dist/firebaseui.css";

const firebaseConfig = {
  apiKey: `${process.env.VUE_APP_FB_KEY}`,
  authDomain: `${process.env.VUE_APP_FB_AUTH}`,
  databaseURL: `${process.env.VUE_APP_RTDB}`,
  projectId: `${process.env.VUE_APP_FB_ID}`,
  storageBucket: "zoomsensedev.appspot.com",
  messagingSenderId: "578675125264",
  appId: "1:578675125264:web:64b8cfb489ae9dc9b6ee95",
  clientId:
    "578675125264-6dag3dtbl96ua2je859l0bskv1pivvd5.apps.googleusercontent.com",
  measurementId: "G-6TWNM5SD5P",
  functions_url: "https://us-central1-zoomsensedev.cloudfunctions.net",
};

firebase.initializeApp(firebaseConfig);

export const db = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();
export const functions = firebase.functions();
export const config = firebaseConfig;
export const fbUi = new firebaseui.auth.AuthUI(auth);
export const googleProvider = firebase.auth.GoogleAuthProvider;
