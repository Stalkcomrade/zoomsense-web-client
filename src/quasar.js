import Vue from "vue";

import "./styles/quasar.sass";
import iconSet from "quasar/icon-set/fontawesome-v5.js";
import "@quasar/extras/roboto-font/roboto-font.css";
import "@quasar/extras/material-icons/material-icons.css";
import "@quasar/extras/fontawesome-v5/fontawesome-v5.css";
import { Quasar, Notify } from "quasar";

Vue.use(Quasar, {
  config: {
    brand: {
      primary: "#4082AD",
      secondary: "#358462",
    },
    notify:{
      
    }
  },
  extras: ["material-icons"],
  components: {
    /* not needed if importStrategy is not 'manual' */
  },
  directives: {
    /* not needed if importStrategy is not 'manual' */
  },
  iconSet: iconSet,
  plugins:{
    Notify
  }
});
